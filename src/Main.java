import models.Cliente;
import models.Ficha;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        // implementación de pilas
        Stack<Ficha> pila = new Stack<>();

        pila.push(new Ficha("Amarillo", 45));
        pila.push(new Ficha("Azul", 12));
        pila.push(new Ficha("Rojo", 23));

        try {
            imprimirFichas(pila);
            pila.pop();
            System.out.println("Tamaño de pila: " + pila.size());
            System.out.println(pila.peek());
        } catch (EmptyStackException e) {
            System.out.println("La pila está vacía ");
        }

        // implementación de las colas
        Queue<Cliente> clientes = new LinkedList<>();
        System.out.println("\n-------------------Colas-------------------");

        clientes.add(new Cliente(0, "Nicolás", "564564", "3124587452"));
        clientes.add(new Cliente(1, "Santiago", "45631", "3125478523"));
        clientes.add(new Cliente(2, "María", "564432", "3253155613"));
        clientes.add(new Cliente(3, "Natalia", "89745", "3165132245"));
        clientes.add(new Cliente(4, "Juan", "598465", "3154751236"));
        clientes.add(new Cliente(5, "Andrés", "645123", "3148941696"));
        clientes.add(new Cliente(6, "Carlos", "49683", "3001574268"));
        clientes.add(new Cliente(7, "Isabela", "5132", "3168521597"));
        clientes.add(new Cliente(8, "Laura", "23135", "3172001555"));
        clientes.add(new Cliente(9, "Vanessa", "78943", "3183210215"));
        clientes.add(new Cliente(10, "Felipe", "87132", "3009632541"));

        System.out.println("\nPrimer cliente: " + clientes.peek());
        System.out.println("Tamaño de la cola: " + clientes.size());
        clientes.poll();
        System.out.println("Tamaño de la cola: " + clientes.size());
        imprimirClientes(clientes);
    }

    public static void imprimirFichas(Stack<Ficha> pila) {
        System.out.println("Fichas [");
        for (Ficha ficha : pila) {
            System.out.println("\t" + ficha.toString());
        }
        System.out.println("]\n");
    }

    public static void imprimirClientes(Queue<Cliente> cola) {
        System.out.println("Clientes [");
        Iterator<Cliente> cliente = cola.iterator();
        while (cliente.hasNext()){
            System.out.println("\t" + cliente.next());
        }
        System.out.println("]\n");
    }
}
