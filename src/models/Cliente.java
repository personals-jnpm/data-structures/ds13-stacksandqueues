package models;

public class Cliente {
    private int idCliente;
    private String nombre;
    private String cedula;
    private String contacto;

    public Cliente(int idCliente, String nombre, String cedula, String contacto) {
        this.idCliente = idCliente;
        this.nombre = nombre;
        this.cedula = cedula;
        this.contacto = contacto;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    @Override
    public String toString() {
        return "{" +
                "id: " + idCliente +
                ", nombre: " + nombre +
                ", cedula: " + cedula +
                ", contacto: " + contacto +
                '}';
    }
}
